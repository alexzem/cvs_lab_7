package com.company;

public class Main {

    public static void main(String[] args) {

        //Vars                  //Init commit
        Boolean bob = true;     //Commit 7
        Double dob = 1.1;       //Commit 7

        System.out.println("New ID = " + bob);   //Commit 7
        System.out.println("New Number = " + dob++);   //Commit 7

        String s = "Hi hi hi";        //Commit 8
        String n = "Name";            //Commit 9

        System.out.println(s + " " + n);    //Commit 6
        System.out.println(s + " now " + n);    //Commit 8

        System.out.println("Its commit with " + n + "Commit 9");    //Commit 9

        String asd = "Name";            //Commit 10
        System.out.println("Its commit with " + asd + "Commit 10");    //Commit 10
    }
}
